# HTML5/CSS3 Simple handyman website template
Screenshots:
* [Screenshot of index page](screenshot-index.png)

## Built With

HTML5/CSS3 <br>
jQuery

## Authors

* **Mantas Salasevicius** - *Developer* - [Portfolio](https://mantas.dev), [Gitlab](https://gitlab.com/mantas.dev)

## Date of development

2017

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details